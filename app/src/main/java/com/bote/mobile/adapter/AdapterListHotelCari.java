package com.bote.mobile.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bote.mobile.DetailHotelActivity;
import com.bote.mobile.R;
import com.bote.mobile.modelData.ModelHotel;

import java.util.ArrayList;

public class AdapterListHotelCari extends RecyclerView.Adapter<AdapterListHotelCari.KategoriHolder> {
    Activity context;
    private ArrayList<ModelHotel> data;

    public AdapterListHotelCari(ArrayList<ModelHotel> modelKamars, Activity context) {
        this.data = modelKamars;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_list_hotel, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        holder.ln_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailHotelActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public TextView tv_nama_hotel;
        public TextView tv_harga;
        public TextView tv_harga_lama;
        public RatingBar rt_rating;
        public ImageView img_kamar;
        public LinearLayout ln_detail;
        public KategoriHolder(View itemView) {
            super(itemView);
            tv_nama_hotel =  itemView.findViewById(R.id.ilh_tv_namahotel);
            ln_detail =  itemView.findViewById(R.id.ilh_detail);
            tv_harga =  itemView.findViewById(R.id.ilh_tv_harga);
            tv_harga_lama =  itemView.findViewById(R.id.ilh_tv_harga_lama);
            img_kamar =  itemView.findViewById(R.id.ilh_img_hotel);
            rt_rating =  itemView.findViewById(R.id.ilh_ratbar_ratinghotel);

        }
    }
}
