package com.bote.mobile.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bote.mobile.DetailPemesananActivity;
import com.bote.mobile.R;
import com.bote.mobile.modelData.ModelPesanan;

import java.util.ArrayList;

public class AdapterPesanan extends RecyclerView.Adapter<AdapterPesanan.KategoriHolder> {
    Activity context;
    private ArrayList<ModelPesanan> data;

    public AdapterPesanan(ArrayList<ModelPesanan> kotaAwal, Activity context) {
        this.data = kotaAwal;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_riwayat_pemesanan, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        holder.lin_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailPemesananActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public LinearLayout lin_detail;
        public KategoriHolder(View itemView) {
            super(itemView);
            lin_detail = itemView.findViewById(R.id.rrp_lin_detail);

        }
    }
}
