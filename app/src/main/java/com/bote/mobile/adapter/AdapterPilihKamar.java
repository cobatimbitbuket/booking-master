package com.bote.mobile.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bote.mobile.R;
import com.bote.mobile.helper.HelperModel;
import com.bote.mobile.modelData.ModelNomorKamar;

import java.util.ArrayList;

public class AdapterPilihKamar extends RecyclerView.Adapter<AdapterPilihKamar.PilihKamarHolder> {
    Activity context;
    private ArrayList<ModelNomorKamar> data;

    public AdapterPilihKamar(ArrayList<ModelNomorKamar> kotaAwal, Activity context) {
        this.data = kotaAwal;
        this.context = context;
    }

    @Override
    public PilihKamarHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_nomorkamar, parent, false);
        return new PilihKamarHolder(view);
    }

    @Override
    public void onBindViewHolder(PilihKamarHolder holder, final int position) {
        holder.tv_nomorkamar.setText(data.get(position).getNama_kamar());


        if (HelperModel.getModelNomorKamar() != null){
            if (data.get(position).getId_kamar().equals(HelperModel.getModelNomorKamar().id_kamar)){
                holder.tv_nomorkamar.setBackgroundResource(R.color.colorPrimaryDark);
                holder.tv_nomorkamar.setTextColor(Color.parseColor("#FFFFFF"));
            }else {
                setview(holder, position);
            }

        }else {
            setview(holder, position);
        }

        holder.tv_nomorkamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).getStatus_kamar().equals("1")){
                    HelperModel.setModelNomorKamar(data.get(position));
                    context.onBackPressed();
                }


            }
        });
    }

    private void setview(PilihKamarHolder holder, int position) {
        if (data.get(position).getStatus_kamar().equals("1")){
            holder.tv_nomorkamar.setBackgroundResource(R.color.colorPrimary);
            holder.tv_nomorkamar.setTextColor(Color.parseColor("#FFFFFF"));

        }else {
            holder.tv_nomorkamar.setBackgroundResource(R.color.putih);
            holder.tv_nomorkamar.setTextColor(Color.parseColor("#0368BA"));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class PilihKamarHolder extends RecyclerView.ViewHolder {
        public TextView tv_nomorkamar;

        public PilihKamarHolder(View itemView) {
            super(itemView);
            tv_nomorkamar = itemView.findViewById(R.id.rn_tv_nomorkamar);

        }
    }
}
