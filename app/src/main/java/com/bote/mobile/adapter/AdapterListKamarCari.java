package com.bote.mobile.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bote.mobile.DetailKamarActivity;
import com.bote.mobile.R;
import com.bote.mobile.modelData.ModelKamar;

import java.util.ArrayList;

public class AdapterListKamarCari extends RecyclerView.Adapter<AdapterListKamarCari.KategoriHolder> {
    Activity context;
    private ArrayList<ModelKamar> data;

    public AdapterListKamarCari(ArrayList<ModelKamar> modelkamr, Activity context) {
        this.data = modelkamr;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_list_kamar, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        holder.btn_kamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailKamarActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
       public TextView nama_kamar;
       public TextView hargalama_kamar;
       public TextView hargasaatini_kamar;
       public ImageView img_kamar;
       public Button btn_kamar;
        public KategoriHolder(View itemView) {
            super(itemView);
            btn_kamar = itemView.findViewById(R.id.ilk_btn_pilih);
            hargasaatini_kamar = itemView.findViewById(R.id.ilk_tv_harga);
            hargalama_kamar = itemView.findViewById(R.id.ilk_tv_harga_lama);
            nama_kamar = itemView.findViewById(R.id.ilk_tv_namakamar);

        }
    }
}
