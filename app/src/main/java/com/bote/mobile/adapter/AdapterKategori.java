package com.bote.mobile.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bote.mobile.R;
import com.bote.mobile.modelData.ModelKategori;

import java.util.ArrayList;

public class AdapterKategori extends RecyclerView.Adapter<AdapterKategori.KategoriHolder> {
    Activity context;
    private ArrayList<ModelKategori> data;

    public AdapterKategori(ArrayList<ModelKategori> kotaAwal, Activity context) {
        this.data = kotaAwal;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_kategori_kamar, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        holder.judul.setText(data.get(position).getNama_kategori());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public TextView judul;
        public ImageView room_img;
        public KategoriHolder(View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.rkk_tv_typekamar);
            room_img = itemView.findViewById(R.id.rkk_img_kategori);


        }
    }
}
