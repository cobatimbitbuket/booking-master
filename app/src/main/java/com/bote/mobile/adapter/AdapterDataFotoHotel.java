package com.bote.mobile.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bote.mobile.R;

import java.util.ArrayList;

public class AdapterDataFotoHotel extends RecyclerView.Adapter<AdapterDataFotoHotel.KategoriHolder> {
    Activity context;
    private ArrayList<String> data;

    public AdapterDataFotoHotel(ArrayList<String> kotaAwal, Activity context) {
        this.data = kotaAwal;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_datafotohotel, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        if (position == 4){
            holder.rd_tv_detail.setVisibility(View.VISIBLE);
        }else {
            holder.rd_tv_detail.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public ImageView rd_img_data;
        public TextView rd_tv_detail;
        public KategoriHolder(View itemView) {
            super(itemView);
            rd_img_data = itemView.findViewById(R.id.rd_img_data);
            rd_tv_detail = itemView.findViewById(R.id.rd_tv_detail);

        }
    }
}
