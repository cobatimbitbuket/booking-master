package com.bote.mobile.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bote.mobile.DetailPromoActivity;
import com.bote.mobile.R;
import com.bote.mobile.modelData.ModelPromo;

import java.util.ArrayList;

public class AdapterPromo extends RecyclerView.Adapter<AdapterPromo.KategoriHolder> {
    Activity context;
    private ArrayList<ModelPromo> data;

    public AdapterPromo(ArrayList<ModelPromo> kotaAwal, Activity context) {
        this.data = kotaAwal;
        this.context = context;
    }

    @Override
    public KategoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_promo, parent, false);
        return new KategoriHolder(view);
    }

    @Override
    public void onBindViewHolder(KategoriHolder holder, final int position) {
        holder.tv_namapromo.setText(data.get(position).getNama_promo());
        holder.card_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailPromoActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KategoriHolder extends RecyclerView.ViewHolder {
        public TextView tv_namapromo;
        public CardView card_detail;
        public KategoriHolder(View itemView) {
            super(itemView);
            tv_namapromo = itemView.findViewById(R.id.rp_tv_judulpromo);
            card_detail = itemView.findViewById(R.id.rp_card_detail);

        }
    }
}
