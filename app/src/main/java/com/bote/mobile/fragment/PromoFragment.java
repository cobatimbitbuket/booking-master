package com.bote.mobile.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bote.mobile.R;
import com.bote.mobile.adapter.AdapterPromo;
import com.bote.mobile.modelData.ModelPromo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoFragment extends Fragment {


    private LinearLayout fpLnLoading;
    private ProgressBar fpPrgress;
    private TextView fpTvMessage;
    private Button fpBtnReload;
    private RecyclerView fpRecyData;

    public PromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promo, container, false);
        initView(view);
        setupketegori();
        return view;
    }

    private void setupketegori() {
        ArrayList<ModelPromo> araymodel = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ModelPromo modelKategori = new ModelPromo();
            modelKategori.setNama_promo("ini judul promo buka nama promo"+ String.valueOf(i+1) +" Bed "+ i);
            araymodel.add(modelKategori);
        }
        fpRecyData.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        fpRecyData.setAdapter(new AdapterPromo(araymodel, getActivity()));

    }

    private void initView(View view) {
        fpLnLoading = (LinearLayout) view.findViewById(R.id.fp_ln_loading);
        fpPrgress = (ProgressBar) view.findViewById(R.id.fp_prgress);
        fpTvMessage = (TextView) view.findViewById(R.id.fp_tv_message);
        fpBtnReload = (Button) view.findViewById(R.id.fp_btn_reload);
        fpRecyData = (RecyclerView) view.findViewById(R.id.fp_recy_data);
    }
}
