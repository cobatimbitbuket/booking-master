package com.bote.mobile.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bote.mobile.CariHotelActivity;
import com.bote.mobile.R;
import com.bote.mobile.adapter.AdapterPromoHome;
import com.bote.mobile.modelData.ModelPromo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewMainFragment extends Fragment {


    private SearchView fnmSearchvMain;
    private CardView fnmCardPlace;
    private TextView fnmTvTanggalin;
    private TextView fnmTvTanggalout;
    private LinearLayout fnmLnJmlkamar;
    private LinearLayout fnmLnJmltamu;
    private Button fnmBtnCari;
    private LinearLayout fnmLnLoadingPromo;
    private ProgressBar fnmPrgressPromo;
    private TextView fnmTvMessagePromo;
    private Button fnmBtnReloadPromo;
    private RecyclerView fnmRecyDataPromo;

    public NewMainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_main, container, false);
        initView(view);
        setupprmo();
        fnmBtnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CariHotelActivity.class));
            }
        });
        return view;
    }

    private void setupprmo() {
        ArrayList<ModelPromo> araymodel = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ModelPromo modelKategori = new ModelPromo();
            modelKategori.setNama_promo("ini judul promo buka nama promo"+ String.valueOf(i+1) +" Bed "+ i);
            araymodel.add(modelKategori);
        }
        fnmRecyDataPromo.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        fnmRecyDataPromo.setAdapter(new AdapterPromoHome(araymodel, getActivity()));

    }

    private void initView(View view) {
        fnmSearchvMain = (SearchView) view.findViewById(R.id.fnm_searchv_main);
        fnmCardPlace = (CardView) view.findViewById(R.id.fnm_card_place);
        fnmTvTanggalin = (TextView) view.findViewById(R.id.fnm_tv_tanggalin);
        fnmTvTanggalout = (TextView) view.findViewById(R.id.fnm_tv_tanggalout);
        fnmLnJmlkamar = (LinearLayout) view.findViewById(R.id.fnm_ln_jmlkamar);
        fnmLnJmltamu = (LinearLayout) view.findViewById(R.id.fnm_ln_jmltamu);
        fnmBtnCari = (Button) view.findViewById(R.id.fnm_btn_cari);
        fnmLnLoadingPromo = (LinearLayout) view.findViewById(R.id.fnm_ln_loading_promo);
        fnmPrgressPromo = (ProgressBar) view.findViewById(R.id.fnm_prgress_promo);
        fnmTvMessagePromo = (TextView) view.findViewById(R.id.fnm_tv_message_promo);
        fnmBtnReloadPromo = (Button) view.findViewById(R.id.fnm_btn_reload_promo);
        fnmRecyDataPromo = (RecyclerView) view.findViewById(R.id.fnm_recy_data_promo);
    }
}
