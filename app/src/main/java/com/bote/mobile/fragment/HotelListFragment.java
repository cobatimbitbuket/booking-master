package com.bote.mobile.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.bote.mobile.CariHotelActivity;
import com.bote.mobile.R;
import com.bote.mobile.adapter.AdapterKategori;
import com.bote.mobile.modelData.ModelKategori;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelListFragment extends Fragment {


    private CardView fhlCardCari;
    private ImageSlider fhlSlider;
    private RecyclerView fhlRecyKategori;

    public HotelListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hotel_list, container, false);
        initView(view);
        fhlCardCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CariHotelActivity.class));
            }
        });
        setupketegori();
        setupsilder();
        return view;
    }

    private void setupsilder() {
        ArrayList<SlideModel> arrayslider = new ArrayList<>();
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        fhlSlider.setImageList(arrayslider);
    }

    private void setupketegori() {
        ArrayList<ModelKategori> araymodel = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ModelKategori modelKategori = new ModelKategori();
            modelKategori.setNama_kategori("Room Type "+ String.valueOf(i+1) +" Bed "+ i);
            araymodel.add(modelKategori);
        }
        fhlRecyKategori.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        fhlRecyKategori.setAdapter(new AdapterKategori(araymodel, getActivity()));

    }

    private void initView(View view) {
        fhlCardCari = (CardView) view.findViewById(R.id.fhl_card_cari);
        fhlSlider = (ImageSlider) view.findViewById(R.id.fhl_slider);
        fhlRecyKategori = (RecyclerView) view.findViewById(R.id.fhl_recy_kategori);
    }
}
