package com.bote.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bote.mobile.helper.HelperModel;

public class IsiDataActivity extends AppCompatActivity {

    private TextView idTvNamahotel;
    private TextView idTvDetail;
    private TextView idTanggalcheckin;
    private TextView idJamcheckin;
    private TextView idTanggalcheckout;
    private TextView idJamcheckout;
    private TextView idNamakamar;
    private TextView idTypekasur;
    private TextView idTamu;
    private TextView idTvNamapemesan;
    private TextView idTvNamapemesanEdit;
    private TextView idTvNamapemesanDetail;
    private Button idBtnLanjut;
    private TextView idTvNomorkamar;
    private TextView idTvNomorkamarEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_data);
        initView();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Isi Data");

        idTvNomorkamarEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IsiDataActivity.this, PilihKamarActivity.class));
            }
        });

        idBtnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IsiDataActivity.this, ReviewSebelumBayarActivity.class));
            }
        });
    }

    private void initView() {
        idTvNamahotel = (TextView) findViewById(R.id.id_tv_namahotel);
        idTvDetail = (TextView) findViewById(R.id.id_tv_detail);
        idTanggalcheckin = (TextView) findViewById(R.id.id_tanggalcheckin);
        idJamcheckin = (TextView) findViewById(R.id.id_jamcheckin);
        idTanggalcheckout = (TextView) findViewById(R.id.id_tanggalcheckout);
        idJamcheckout = (TextView) findViewById(R.id.id_jamcheckout);
        idNamakamar = (TextView) findViewById(R.id.id_namakamar);
        idTypekasur = (TextView) findViewById(R.id.id_typekasur);
        idTamu = (TextView) findViewById(R.id.id_tamu);
        idTvNamapemesan = (TextView) findViewById(R.id.id_tv_namapemesan);
        idTvNamapemesanEdit = (TextView) findViewById(R.id.id_tv_namapemesan_edit);
        idTvNamapemesanDetail = (TextView) findViewById(R.id.id_tv_namapemesan_detail);
        idBtnLanjut = (Button) findViewById(R.id.id_btn_lanjut);
        idTvNomorkamar = (TextView) findViewById(R.id.id_tv_nomorkamar);
        idTvNomorkamarEdit = (TextView) findViewById(R.id.id_tv_nomorkamar_edit);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (HelperModel.getModelNomorKamar() != null){
            idTvNomorkamar.setText(HelperModel.getModelNomorKamar().getNama_kamar());
        }
    }
}
