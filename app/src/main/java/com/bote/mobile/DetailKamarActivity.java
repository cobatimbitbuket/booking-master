package com.bote.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;

import java.util.ArrayList;

public class DetailKamarActivity extends AppCompatActivity {


    private ImageSlider imageSlider;
    private TextView adkTvNamakamar;
    private RatingBar adkRatbarRatingkamar;
    private TextView adkTvLihatreview;
    private TextView adkTvHargakamar;
    private TextView adkTvDetailkamar;
    private Button adhBtnBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kamar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Nama Kamar");
        initView();
        setupsilder();


        adhBtnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailKamarActivity.this, IsiDataActivity.class));
            }
        });
    }

    private void setupsilder() {
        ArrayList<SlideModel> arrayslider = new ArrayList<>();
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        arrayslider.add(new SlideModel(R.drawable.dummy_room));
        imageSlider.setImageList(arrayslider);
    }


    private void initView() {
        imageSlider = (ImageSlider) findViewById(R.id.image_slider);
        adkTvNamakamar = (TextView) findViewById(R.id.adk_tv_namakamar);
        adkRatbarRatingkamar = (RatingBar) findViewById(R.id.adk_ratbar_ratingkamar);
        adkTvLihatreview = (TextView) findViewById(R.id.adk_tv_lihatreview);
        adkTvHargakamar = (TextView) findViewById(R.id.adk_tv_hargakamar);
        adkTvDetailkamar = (TextView) findViewById(R.id.adk_tv_detailkamar);
        adhBtnBooking = (Button) findViewById(R.id.adh_btn_booking);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
