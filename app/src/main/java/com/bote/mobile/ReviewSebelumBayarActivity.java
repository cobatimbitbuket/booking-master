package com.bote.mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReviewSebelumBayarActivity extends AppCompatActivity {

    private TextView arTvNamahotel;
    private TextView arTvTanggalcheckin;
    private TextView arTvHarijamcheckin;
    private TextView arTvTanggalcheckout;
    private TextView arTvHarijamcheckout;
    private TextView arTvDurasi;
    private TextView arTvNamakamar;
    private TextView arTvKapasitas;
    private TextView arTvKbjp;
    private TextView arTvNamapemesan;
    private TextView arTvPermintaankhusus;
    private TextView arTvDetailrincianharganamakamar;
    private TextView arTvHargakamar;
    private TextView arTvKodeunik;
    private TextView arTvPajak;
    private TextView arTvTotaldibayar;
    private Button idBtnLanjut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_sebelum_bayar);
        initView();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Review Pesanan");
        idBtnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReviewSebelumBayarActivity.this, MetodePembayaranActivity.class));
            }
        });
    }

    private void initView() {
        arTvNamahotel = (TextView) findViewById(R.id.ar_tv_namahotel);
        arTvTanggalcheckin = (TextView) findViewById(R.id.ar_tv_tanggalcheckin);
        arTvHarijamcheckin = (TextView) findViewById(R.id.ar_tv_harijamcheckin);
        arTvTanggalcheckout = (TextView) findViewById(R.id.ar_tv_tanggalcheckout);
        arTvHarijamcheckout = (TextView) findViewById(R.id.ar_tv_harijamcheckout);
        arTvDurasi = (TextView) findViewById(R.id.ar_tv_durasi);
        arTvNamakamar = (TextView) findViewById(R.id.ar_tv_namakamar);
        arTvKapasitas = (TextView) findViewById(R.id.ar_tv_kapasitas);
        arTvKbjp = (TextView) findViewById(R.id.ar_tv_kbjp);
        arTvNamapemesan = (TextView) findViewById(R.id.ar_tv_namapemesan);
        arTvPermintaankhusus = (TextView) findViewById(R.id.ar_tv_permintaankhusus);
        arTvDetailrincianharganamakamar = (TextView) findViewById(R.id.ar_tv_detailrincianharganamakamar);
        arTvHargakamar = (TextView) findViewById(R.id.ar_tv_hargakamar);
        arTvKodeunik = (TextView) findViewById(R.id.ar_tv_kodeunik);
        arTvPajak = (TextView) findViewById(R.id.ar_tv_pajak);
        arTvTotaldibayar = (TextView) findViewById(R.id.ar_tv_totaldibayar);
        idBtnLanjut = (Button) findViewById(R.id.id_btn_lanjut);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
