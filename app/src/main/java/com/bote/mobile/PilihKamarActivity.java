package com.bote.mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.bote.mobile.adapter.AdapterPilihKamar;
import com.bote.mobile.modelData.ModelNomorKamar;

import java.util.ArrayList;

public class PilihKamarActivity extends AppCompatActivity {

    private RecyclerView apkRecyMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kamar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pilih Nomor Kamar Anda");
        initView();
        setdatadummy();
    }

    public void setdatadummy() {
        ArrayList<ModelNomorKamar> arraynomorkamar = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            ModelNomorKamar modelNomorKamar = new ModelNomorKamar();
            modelNomorKamar.setId_kamar(String.valueOf(i));
            modelNomorKamar.setNama_kamar(String.valueOf(i + 1));
            if (i > 3 && i != 5) {
                modelNomorKamar.setStatus_kamar("1");
            } else {
                modelNomorKamar.setStatus_kamar("0");
            }
            arraynomorkamar.add(modelNomorKamar);
        }
        apkRecyMain.setLayoutManager(new GridLayoutManager(PilihKamarActivity.this, 4));
        apkRecyMain.setAdapter(new AdapterPilihKamar(arraynomorkamar, PilihKamarActivity.this));


    }

    private void initView() {
        apkRecyMain = (RecyclerView) findViewById(R.id.apk_recy_main);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
