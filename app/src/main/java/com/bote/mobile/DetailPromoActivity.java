package com.bote.mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailPromoActivity extends AppCompatActivity {

    private ImageView adpImgPromo;
    private TextView adpTvJudulpromo;
    private TextView adpTvKodepromo;
    private TextView adpTvExp;
    private TextView adpTvCarapakai;
    private TextView adpTvSk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_promo);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Nama Promo");
        initView();
    }

    private void initView() {
        adpImgPromo = (ImageView) findViewById(R.id.adp_img_promo);
        adpTvJudulpromo = (TextView) findViewById(R.id.adp_tv_judulpromo);
        adpTvKodepromo = (TextView) findViewById(R.id.adp_tv_kodepromo);
        adpTvExp = (TextView) findViewById(R.id.adp_tv_exp);
        adpTvCarapakai = (TextView) findViewById(R.id.adp_tv_carapakai);
        adpTvSk = (TextView) findViewById(R.id.adp_tv_sk);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
