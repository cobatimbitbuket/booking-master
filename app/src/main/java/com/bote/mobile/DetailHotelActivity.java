package com.bote.mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.bote.mobile.adapter.AdapterDataFotoHotel;
import com.bote.mobile.adapter.AdapterListKamarCari;
import com.bote.mobile.modelData.ModelKamar;

import java.util.ArrayList;

public class DetailHotelActivity extends AppCompatActivity {

    private TextView adhTvNamahotel;
    private TextView adhTvRating;
    private TextView adhTvUlasan;
    private TextView adhTvDeskripsihotel;
    private RecyclerView adhRecyDatakamar;
    private RecyclerView adhRecyDatafoto;
    private TextView ilhTvLokasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_hotel);
        initView();
        getSupportActionBar().setTitle("Nama Hotel");
        getSupportActionBar().setSubtitle("19 Januari");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        load_dummy_data();
    }



    private void load_dummy_data() {
        ArrayList<ModelKamar> datadummy = new ArrayList<>();
        ArrayList<String> datadummyfoto = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ModelKamar modelKategori = new ModelKamar();
            modelKategori.setNama_hotel("Kamar " + String.valueOf(i + 1));
            datadummy.add(modelKategori);
            datadummyfoto.add("link");

        }
        adhRecyDatafoto.setAdapter(new AdapterDataFotoHotel(datadummyfoto, DetailHotelActivity.this));
        adhRecyDatafoto.setLayoutManager(new LinearLayoutManager(DetailHotelActivity.this, LinearLayoutManager.HORIZONTAL, false));
        adhRecyDatakamar.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        adhRecyDatakamar.setAdapter(new AdapterListKamarCari(datadummy, DetailHotelActivity.this));
    }

    private void initView() {
        adhTvNamahotel = (TextView) findViewById(R.id.adh_tv_namahotel);
        adhTvRating = (TextView) findViewById(R.id.adh_tv_rating);
        adhTvUlasan = (TextView) findViewById(R.id.adh_tv_ulasan);
        adhTvDeskripsihotel = (TextView) findViewById(R.id.adh_tv_deskripsihotel);
        adhRecyDatakamar = (RecyclerView) findViewById(R.id.adh_recy_datakamar);
        adhRecyDatafoto = (RecyclerView) findViewById(R.id.adh_recy_datafoto);
        ilhTvLokasi = (TextView) findViewById(R.id.ilh_tv_lokasi);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
