package com.bote.mobile.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.bote.mobile.helper.sharedpreferences.ModelPreferences;

public class PreferenceApp {
    public static PreferenceApp preferenceApp;
    public  SharedPreferences preferences;


    //DATA MODEL PREFERENCES
    public static String APP_PREFERENCE;

    public static String PESANAN_NOMORKAMAR;

    private PreferenceApp(Context context){
        preferences = context.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
    }

    public void  initPreferences(Context context){
        if (preferenceApp != null){
            preferenceApp = new PreferenceApp(context);
        }
    }

    public static String getPesananNomorkamar() {
        return preferenceApp.preferences.getString(PESANAN_NOMORKAMAR, "");
    }

    public static void setPesananNomorkamar(String pesananNomorkamar) {
        preferenceApp.preferences.edit().putString(PESANAN_NOMORKAMAR, pesananNomorkamar).apply();
    }

}
