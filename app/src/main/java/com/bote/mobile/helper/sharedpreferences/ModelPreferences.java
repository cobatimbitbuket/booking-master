package com.bote.mobile.helper.sharedpreferences;

public class ModelPreferences {
    public static String APP_PREFERENCE;

    public static String getAppPreference() {
        return APP_PREFERENCE;
    }

    public static void setAppPreference(String appPreference) {
        APP_PREFERENCE = appPreference;
    }


}
