package com.bote.mobile.modelData;

public class ModelHotel {
    String id_hotel;
    String nama_hotel;
    String rating_hotel;
    String hargaawal_hotel;
    String hargasaatini_hotel;
    String img_hotel;

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

    public String getNama_hotel() {
        return nama_hotel;
    }

    public void setNama_hotel(String nama_hotel) {
        this.nama_hotel = nama_hotel;
    }

    public String getRating_hotel() {
        return rating_hotel;
    }

    public void setRating_hotel(String rating_hotel) {
        this.rating_hotel = rating_hotel;
    }

    public String getHargaawal_hotel() {
        return hargaawal_hotel;
    }

    public void setHargaawal_hotel(String hargaawal_hotel) {
        this.hargaawal_hotel = hargaawal_hotel;
    }

    public String getHargasaatini_hotel() {
        return hargasaatini_hotel;
    }

    public void setHargasaatini_hotel(String hargasaatini_hotel) {
        this.hargasaatini_hotel = hargasaatini_hotel;
    }

    public String getImg_hotel() {
        return img_hotel;
    }

    public void setImg_hotel(String img_hotel) {
        this.img_hotel = img_hotel;
    }


}
