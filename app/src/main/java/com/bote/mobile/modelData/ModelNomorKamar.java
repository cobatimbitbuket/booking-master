package com.bote.mobile.modelData;

public class ModelNomorKamar {
    public String id_kamar;
    public String nama_kamar;
    public String status_kamar;
    public String getId_kamar() {
        return id_kamar;
    }

    public void setId_kamar(String id_kamar) {
        this.id_kamar = id_kamar;
    }

    public String getNama_kamar() {
        return nama_kamar;
    }

    public void setNama_kamar(String nama_kamar) {
        this.nama_kamar = nama_kamar;
    }

    public String getStatus_kamar() {
        return status_kamar;
    }

    public void setStatus_kamar(String status_kamar) {
        this.status_kamar = status_kamar;
    }


}
