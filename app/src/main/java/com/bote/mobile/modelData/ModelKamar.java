package com.bote.mobile.modelData;

public class ModelKamar {
    String id_hotel;
    String nama_hotel;

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

    public String getNama_hotel() {
        return nama_hotel;
    }

    public void setNama_hotel(String nama_hotel) {
        this.nama_hotel = nama_hotel;
    }

    public String getImg_hotel() {
        return img_hotel;
    }

    public void setImg_hotel(String img_hotel) {
        this.img_hotel = img_hotel;
    }

    String img_hotel;

}
