package com.bote.mobile.mvp.model;

public interface ILogin {
    String getEmail();
    String getPassword();
    int isValidData();
}
