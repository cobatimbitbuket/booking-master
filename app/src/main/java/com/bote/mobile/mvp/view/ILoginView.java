package com.bote.mobile.mvp.view;

public interface ILoginView {

    void onLoginSuccess(String message);
    void onLoginError(String message);
    void onLoginLoding(String message);
}
