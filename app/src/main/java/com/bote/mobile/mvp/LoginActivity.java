package com.bote.mobile.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bote.mobile.NewMainActivity;
import com.bote.mobile.R;
import com.bote.mobile.mvp.presenter.ILoginPresenter;
import com.bote.mobile.mvp.presenter.LoginPresenter;
import com.bote.mobile.mvp.view.ILoginView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    private EditText loginEdtEmail;
    private EditText loginEdtPassword;
    private Button loginBtnLogin;

    ILoginPresenter iLoginPresenter;
    private TextView loginTvNameapp;
    private LinearLayout loginLinData;
    private Button loginBtnRegister;
    private ImageView loginImgNameapp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        loadAnimation();
        getSupportActionBar().hide();

        iLoginPresenter = new LoginPresenter(this);
        loginBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email_tx = loginEdtEmail.getText().toString();
                String password_tx = loginEdtPassword.getText().toString();
                iLoginPresenter.onLogin(email_tx, password_tx);
            }
        });
    }

    private void initView() {
        loginEdtEmail = (EditText) findViewById(R.id.login_edt_email);
        loginEdtPassword = (EditText) findViewById(R.id.login_edt_password);
        loginBtnLogin = (Button) findViewById(R.id.login_btn_login);
        loginTvNameapp = (TextView) findViewById(R.id.login_tv_nameapp);
        loginLinData = (LinearLayout) findViewById(R.id.login_lin_data);
        loginBtnRegister = (Button) findViewById(R.id.login_btn_register);
        loginImgNameapp = (ImageView) findViewById(R.id.login_img_nameapp);
    }

    @Override
    public void onLoginSuccess(String message) {
        Intent intent = new Intent(LoginActivity.this, NewMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onLoginError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginLoding(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void loadAnimation() {
        YoYo.with(Techniques.BounceInUp)
                .duration(6000)
                .playOn(loginTvNameapp);
        YoYo.with(Techniques.BounceInUp)
                .duration(6000)
                .playOn(loginImgNameapp);
    }
}
