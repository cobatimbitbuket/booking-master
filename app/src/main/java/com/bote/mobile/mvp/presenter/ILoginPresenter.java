package com.bote.mobile.mvp.presenter;

public interface ILoginPresenter {

    void onLogin(String email, String password);
}
