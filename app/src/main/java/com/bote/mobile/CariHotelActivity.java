package com.bote.mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bote.mobile.adapter.AdapterListHotelCari;
import com.bote.mobile.modelData.ModelHotel;

import java.util.ArrayList;

public class CariHotelActivity extends AppCompatActivity {

    private TextView amTvTanggalIn;
    private TextView amTvTanggalOut;
    private CardView amCardCari;
    private CardView amCardFilter;
    private LinearLayout amLnLoading;
    private ProgressBar amPrgress;
    private TextView amTvMessage;
    private Button amBtnReload;
    private RecyclerView amRecyData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carihotel);
        initView();
        getSupportActionBar().setTitle("Singkawang");
        getSupportActionBar().setSubtitle("19 Januari");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        load_dummy_data();
    }

    private void load_dummy_data() {
        ArrayList<ModelHotel> datadummy = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ModelHotel modelKategori = new ModelHotel();
            modelKategori.setNama_hotel("Hotel "+ String.valueOf(i+1));
            datadummy.add(modelKategori);
        }
        amRecyData.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        amRecyData.setAdapter(new AdapterListHotelCari( datadummy, CariHotelActivity.this));
    }

    private void initView() {
        amLnLoading = (LinearLayout) findViewById(R.id.am_ln_loading);
        amPrgress = (ProgressBar) findViewById(R.id.am_prgress);
        amTvMessage = (TextView) findViewById(R.id.am_tv_message);
        amBtnReload = (Button) findViewById(R.id.am_btn_reload);
        amRecyData = (RecyclerView) findViewById(R.id.am_recy_data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
